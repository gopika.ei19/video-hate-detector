<?php

return [
    'videos_folder' => env('VIDEOS_FOLDER', '/var/www/vhd-videos/'),
    'duration_filter' => [0, 300],
    'min_annotation_video' => 3,
    //This configuration defines how long the video will be locked once it is sent to an user to be evaluated. It multiplies the duration of the video.
    'video_lock_factor' => 3,
];
