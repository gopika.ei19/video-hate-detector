<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'google' => [
        'client_id' => '292328321061-v7fqflgpav8t7cb2u9nogg0461u389d6.apps.googleusercontent.com',
        'project_id' => 'offensive-video-detector',
        'auth_uri' => 'https://accounts.google.com/o/oauth2/auth',
        'token_uri' => 'https://www.googleapis.com/oauth2/v3/token',
        'auth_provider_x509_cert_url' => 'https://www.googleapis.com/oauth2/v1/certs',
        'client_secret' => 'xCsdvvOEXF3ceTaw4B2qDUeT',
        'redirect' => '/login/google/callback',
    ],
    
    'facebook' => [
        'client_id' => '2147187698879507',
        'auth_uri' => 'https://inf.ufrgs.br/~csalcantara/video-hate-detector/facebook-login.html',
        'client_secret' => '7221e99793112f1e925987936f548473',
        'redirect' => '/login/facebook/callback'
    ],

];
