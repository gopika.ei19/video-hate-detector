<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeTag extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'moment',
    ];
    
    public function annotation(){
        return $this->belongsTo('App\Annotation');
    }
}
