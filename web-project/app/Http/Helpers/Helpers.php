<?php

namespace App\Http\Helpers;

class Helpers {

    /**
     * Generate a url for the application.
     *
     * @param  string  $path
     * @param  mixed   $parameters
     * @param  bool    $secure
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function url($path = null, $parameters = [], $secure = null) {
        return url(static::request_path($path), $parameters, $secure);
    }

    /**
     * Generate a HTTPS url for the application.
     *
     * @param  string  $path
     * @param  mixed   $parameters
     * @return string
     */
    public static function secure_url($path, $parameters = []) {
        return static::url($path, $parameters, true);
    }

    /**
     * Simply attaches the locale to the beginning of the path.
     * @param string $path
     * @param string $locale
     * @return string
     */
    public static function request_path($path = null, $locale = null) {
        $path = (string) $path;
        if (strlen($path) > 0 && $path[0] !== '/') {
            $path = '/' . $path;
        }
        
        if($locale === null){
            $locale = app()->getLocale();
        }

        return $locale . $path;
    }

    public static function request_is($path) {
        if (is_array($path)) {
            $pathWitoutLocale = substr(request()->path(), strlen(app()->getLocale()) + 1);
            return in_array($pathWitoutLocale, $path);
        } else {
            return request()->is(Helpers::request_path($path));
        }
    }
    
    public static function get_current_url_with_locale($localeKey){
        $baseWithLocale = static::url('');
        $base = substr($baseWithLocale, 0, strlen($baseWithLocale) - strlen(app()->getLocale()));
        $fullUrl = request()->fullUrl();
        $path = substr($fullUrl, strlen($base));
        if(strpos($path, '/') === FALSE){
            $emptyCases = array_merge(array_keys(config('app.available_locales')),
                    ['pt-br', '']);
            if(in_array($path, $emptyCases)){
                $urlWithoutLocale = '';
            } else {
                $urlWithoutLocale = '/' . $path;
            }
        } else {
            $urlWithoutLocale = substr($path, strpos($path, '/'));
        }
        $finalUrl = $base . $localeKey . $urlWithoutLocale;
        return $finalUrl;
    }

}
