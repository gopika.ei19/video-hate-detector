<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\YTVideoSnippet;
use App\Annotation;

class StatisticController extends Controller {

    public function index(Request $request) {
        if (!(Auth::check() && Auth::user()->is_admin)) {
            abort(404);
        }
        
        $statistics = [
            'pt' => [],
            'en' => []
        ];
        
        foreach ($statistics as $language => $languageStatistic) {
            $mainQuery = YTVideoSnippet::where('default_audio_language', 'like', $language . '%')
                    ->join('content_details', 'yt_video_snippets.id', '=', 'content_details.yt_video_snippets_id');
                    //->whereBetween('content_details.duration', config('vhd.duration_filter'));
            $statistics[$language]['total'] = $mainQuery->count();
            
            $mainQuery->whereBetween('content_details.duration', config('vhd.duration_filter'));
            $statistics[$language]['totalFiltered'] = $mainQuery->count();
            
            $readyQuery = clone $mainQuery;
            $statistics[$language]['totalReady'] = $readyQuery->where('ready', 1)->count();
            
            $refusedQuery = clone $mainQuery;
            $statistics[$language]['totalRefused'] = $refusedQuery->where('ready', 0)->count();
            
            $annotationsMainQuery = Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                    ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                    ->where('yt_video_snippets.default_audio_language', 'like', $language . '%')
                    ->where('yt_video_snippets.ready', '=', 1)
                    ->whereBetween('content_details.duration', config('vhd.duration_filter'));
            $statistics[$language]['totalAnnotations'] = $annotationsMainQuery->count();
            
            $offensiveQuery = clone $annotationsMainQuery;
            
            $annotationsCounterQuery = $annotationsMainQuery->select(DB::raw('count(*) as annotations_counter, video_id'))
                    ->groupBy('video_id');
            $videosCounterQuery = DB::table(DB::raw('(' . $annotationsCounterQuery->toSql() . ') as annotations_per_video'))
                    ->mergeBindings($annotationsCounterQuery->getQuery())
                    ->select(DB::raw('count(*) as videos_counter, annotations_counter'))
                    ->groupBy('annotations_counter');
            Log::debug($videosCounterQuery->toSql());
            $annotationCountersDB = $videosCounterQuery->pluck('videos_counter', 'annotations_counter');
            $annotationCounters = [];
            $statistics[$language]['totalVideos'] = 0;
            foreach(array_keys($annotationCountersDB->toArray()) as $counter){
                if(isset($annotationCountersDB[$counter])){
                    $annotationCounters[$counter] = $annotationCountersDB[$counter];
                } else {
                    $annotationCounters[$counter] = 0;
                }
                $statistics[$language]['totalVideos'] += $annotationCounters[$counter];
            }
            $statistics[$language]['annotationCounters'] = $annotationCounters;
            
            $offensiveQuery->withCount('timeTags');
            Log::debug($offensiveQuery->toSql());
            $timeTagsQuery = DB::table(DB::raw('(' . $offensiveQuery->toSql() . ') as videos_tags'))
                    ->mergeBindings($offensiveQuery->getQuery())
                    ->select(DB::raw('video_id, '
                            . 'SUM(CASE WHEN time_tags_count > 0 THEN 1 ELSE 0 END) as offensive_counter, '
                            . 'count(*) AS annotations_counter'))
                    ->havingRaw('count(*) >= ' . config('vhd.min_annotation_video'))
                    ->groupBy('video_id');
            Log::debug($timeTagsQuery->toSql());
            $videos = $timeTagsQuery->get();
            $agreement = [
                'agreement_by2' => count($videos->filter(function($value){
                    return $value->offensive_counter >= 2;
                })),
                'agreement_by3' => count($videos->filter(function($value){
                    return $value->offensive_counter >= 3;
                })),
            ];
            $statistics[$language]['agreement'] = $agreement;
            $statistics[$language]['agreementCounter'] = count($videos);
        }
        
        $total = 0;
        $totalFiltered = 0;
        foreach ($statistics as $language => $languageStatistic) {
            $total += $statistics[$language]['total'];
            $totalFiltered += $statistics[$language]['totalFiltered'];
        }
        $statistics['total'] = $total;
        $statistics['totalFiltered'] = $totalFiltered;
        
        Log::debug($statistics);

        return view('statistics')->with($statistics);
    }

}
