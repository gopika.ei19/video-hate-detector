<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\YTVideoSnippet;
use App\Annotation;
use App\TimeTag;
use App\User;

class AnnotationController extends Controller {

    public function getAll(Request $request, $userId = NULL) {
        if (Route::currentRouteName() === 'user_annotations'){ 
            if(!(Auth::check() && Auth::user()->is_admin)) {
                abort(404);
            }
            $userName = User::find($userId)->name;
        } else {
            $userId = $request->user()->id;
            $userName = '';
        }
        $readyVideos = YTVideoSnippet::select('id')
                ->join('content_details', 'yt_video_snippets.id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->where('yt_video_snippets.ready', '=', 1)
                ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                ->pluck('id')
                ->toArray();

        $annotationsMainQuery = Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.ready', '=', 1)
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->whereBetween('content_details.duration', config('vhd.duration_filter'));
        $allPTAnnotationsQuery = clone $annotationsMainQuery;
        $allPTAnnotationsQuery->select(DB::raw('count(*) as annotations_counter, video_id'))
                ->groupBy('video_id');
        $videosPTFullyAnnotatedQuery = DB::table(DB::raw('(' . $allPTAnnotationsQuery->toSql() . ') as annotations_per_video'))
                ->mergeBindings($allPTAnnotationsQuery->getQuery())
                ->select('video_id')
                ->where('annotations_counter', '>=', config('vhd.min_annotation_video'));
        $videosFullyAnnotatedPT = $videosPTFullyAnnotatedQuery->pluck('video_id')->toArray();

        if (Auth::guest()) {
            $annotations = [];
            $possibleVideos = [];
            $userAnnotationsVideosPTIds = [];
        } else {
            $userAnnotationsQuery = clone $annotationsMainQuery;
            $userAnnotationsQuery->select(DB::raw('annotations.*, yt_video_snippets.default_audio_language, content_details.duration'))
                    ->withCount('timeTags')
                    ->where('user_id', '=', $userId);
            $annotations = $userAnnotationsQuery->get();

            $userAnnotationsVideosPTIds = array_filter(array_map(function($element) {
                        if (strtolower(substr($element['default_audio_language'], 0, 2)) === 'en') {
                            return false;
                        } else {
                            return $element['video_id'];
                        }
                    }, $annotations->toArray()));

            $excludeVideos = array_unique(array_merge($userAnnotationsVideosPTIds, $videosFullyAnnotatedPT));
            $possibleVideos = array_diff($readyVideos, $excludeVideos);

            foreach ($annotations as $annotation) {
                $video = new YTVideoSnippet(['id' => $annotation->video_id]);
                $annotation->videoMetadata = $video->getMetadata();
            }
        }

        return view('annotations')->with([
                    'annotations' => $annotations,
                    'myAnnotationsPT' => count($userAnnotationsVideosPTIds),
                    'possibleHelp' => count($possibleVideos),
                    'userName' => $userName,
        ]);
    }
    
    public function getRankingPage(Request $request) {
        return view('ranking')->with([
                    'topAnnotators' => self::getRanking(),
        ]);
    }

    public static function getRanking($limit = NULL) {
        $usersQuery = Annotation::select(
                        DB::raw('users.id, users.name, count(*) as annotations_count, sum(content_details.duration) as watched_time')
                )->join('users', 'users.id', '=', 'annotations.user_id')
                ->join('yt_video_snippets', 'yt_video_snippets.id', '=', 'annotations.video_id')
                ->join('content_details', 'content_details.yt_video_snippets_id', '=', 'annotations.video_id')
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->where('yt_video_snippets.ready', '=', 1)
                ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                ->orderBy('annotations_count', 'desc')
                ->orderBy('watched_time', 'desc')
                ->orderBy('users.created_at')
                ->groupBy('users.id', 'users.name', 'users.created_at')
                ->limit($limit);
        if (Auth::guest() || !Auth::user()->is_admin) {
            $usersQuery->where('users.is_admin', '=', 0);
        }
        $users = $usersQuery->get();

        $topAnnotators = array_filter($users->toArray(), function($user) {
            return $user['annotations_count'] > 0;
        });
        return $topAnnotators;
    }

    public function getAnnotationForm(Request $request) {
        $videos_filters = [];
        if ($request->has('video_language')) {
            $language = $request->video_language;
        } else {
            $language = substr(app()->getLocale(), 0, 2);
        }

        $targetVideo = NULL;
        if (strtolower($language) === 'pt') {
            $videos_filters[] = ['default_audio_language', 'like', $language . '%'];
            $videos_filters[] = ['ready', '=', TRUE];
            $videos_filters[] = ['downloaded', '=', TRUE];

            $user_annotations = Annotation::where('user_id', '=', $request->user()->id)->pluck('video_id');

            $videos = YTVideoSnippet::join('content_details', 'content_details.yt_video_snippets_id', 'yt_video_snippets.id')
                    ->select(DB::raw('yt_video_snippets.*, content_details.duration AS `duration`'))
                    ->where($videos_filters)
                    ->whereNotIn('id', $user_annotations)
                    ->withCount('annotations')
                    ->orderBy('annotations_count', 'desc')
                    ->get();

            foreach ($videos as $video) {
                if ($video->annotations_count < config('vhd.min_annotation_video')) {
                    $expirationDate = time();
                    $lockedVideosCounter = DB::table('locked_videos')
                            ->where([
                                ['expiration_time', '>', date("Y-m-d H:i:s", $expirationDate)],
                                ['yt_video_snippets_id', '=', $video->id],
                                ['user_id', '=', $request->user()->id],
                            ])
                            ->count();
                    if ($lockedVideosCounter === 0) {
                        $lockedVideosCounter = DB::table('locked_videos')
                                ->where([
                                    ['expiration_time', '>', date("Y-m-d H:i:s", $expirationDate)],
                                    ['yt_video_snippets_id', '=', $video->id]
                                ])
                                ->count();
                        if ($lockedVideosCounter + $video->annotations_count < config('vhd.min_annotation_video')) {
                            $targetVideo = $video;
                            $expirationDate += config('vhd.video_lock_factor') * $video->duration;
                            DB::table('locked_videos')->insert([
                                'yt_video_snippets_id' => $video->id,
                                'user_id' => $request->user()->id,
                                'expiration_time' => date("Y-m-d H:i:s", $expirationDate)
                            ]);
                            Log::debug('Video ' . $video->id
                                    . ' locked by user ' . $request->user()->id
                                    . ' expiring on ' . date("Y-m-d H:i:s", $expirationDate));
                            break;
                        }
                    } else {
                        $targetVideo = $video;
                        $expirationDate += config('vhd.video_lock_factor') * $video->duration;
                        DB::table('locked_videos')->where([
                                    ['yt_video_snippets_id', '=', $video->id],
                                    ['user_id', '=', $request->user()->id]
                                ])
                                ->update([
                                    'expiration_time' => date("Y-m-d H:i:s", $expirationDate)
                        ]);
                        Log::debug('Video ' . $video->id
                                . ' already locked by user '
                                . $request->user()->id
                                . '. Expiration time updated to '
                                . date("Y-m-d H:i:s", $expirationDate));
                        break;
                    }
                } else {
                    Log::debug('Video ' . $video->id . ' is already fully annotated ('
                            . $video->annotations_count . ' annotations). Checking the next...');
                }
            }
        }
        if($targetVideo === NULL){
            Log::info('No available for annotation videos found for user ' . $request->user()->id);
        }
        return view('annotation-form')->with([
                    'video' => $targetVideo,
                    'video_language' => $language
        ]);
    }

    public function create(Request $request) {
        $result = [
            'status' => 0,
            'message' => [
                'title' => trans('general.annotation_success_title'),
                'text' => trans('general.annotation_success_text')
            ]
        ];

        $video = YTVideoSnippet::find($request->video_id);
        if ($video === null) {
            $result['status'] = 1;
            $result['message'] = [
                'title' => trans('general.annotation_error_title'),
                'text' => trans('general.annotation_error_text_not_exist')
            ];
            Log::warning('User ' . $request->user()->id
                    . ' tried to annotate a non existing video: ' . $video->id);
            return $result;
        }

        $existingAnnotations = Annotation::where('user_id', '=', $request->user()->id)->pluck('video_id');
        if (in_array($video->id, $existingAnnotations->toArray())) {
            Log::warning('User ' . $request->user()->id
                    . ' tried to annotate the video ' . $video->id
                    . ' already annotated by him.');
            $result['status'] = 2;
            $result['message'] = [
                'title' => trans('general.annotation_error_title'),
                'text' => trans('general.annotation_error_text_already_evaluated')
            ];
            return $result;
        }

        $timeTags = array_map(function($tag) {
            return new TimeTag(['moment' => $tag]);
        }, array_filter(json_decode($request->tags), function($tag) use($video) {
                    return $tag >= 0 || $tag <= $video->contentDetail()->duration;
                }));
        $annotation = new Annotation([
            'user_id' => $request->user()->id,
            'video_id' => $video->id
        ]);
        $annotation->save();
        $annotation->timeTags()->saveMany($timeTags);

        DB::table('locked_videos')->where([
                    ['yt_video_snippets_id', '=', $video->id],
                    ['user_id', '=', $request->user()->id]
                ])
                ->delete();

        Log::info('User ' . $request->user()->id
                . ' annotated the video ' . $video->id
                . ' with ' . count($timeTags) . ' time tags.');

        return response()->json($result);
    }
    
    public static function getAllAnnotationsPerDay(){
        $annotationsDBData = Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.ready', '=', 1)
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") AS annotation_date,'
                . 'COUNT(*) AS counter'))
                ->groupBy('annotation_date')
                ->orderBy('annotation_date', 'ASC')
                ->pluck('counter', 'annotation_date');
        $annotationsData = $annotationsDBData->toArray();

        foreach($annotationsData as $key => $data){
            $annotationsData[$key] = [
                'counter' => $data,
                'annotators' => Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                    ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                    ->join('users', 'annotations.user_id', '=', 'users.id')
                    ->where('yt_video_snippets.ready', '=', 1)
                    ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                    ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                    ->whereDate('annotations.created_at', $key)
                    ->select(DB::raw('users.name as name,'
                    . 'COUNT(*) AS counter'))
                    ->groupBy('users.name')
                    ->orderBy('counter', 'DESC')
                    ->get()
            ];
        }
        return $annotationsData;
    }
    public static function getMinAnnotationsVideoPerDay(){
        $lastMinAnnotationsVideoQuery = Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.ready', '=', 1)
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                ->select(DB::raw('video_id, '
                . 'MAX(DATE_FORMAT(created_at, "%Y-%m-%d")) AS annotation_date,'
                . 'COUNT(*) AS in_counter'))
                ->groupBy('video_id')
                ->orderBy('annotation_date', 'ASC');
        $minAnnotationsVideo = DB::table(DB::raw('(' . $lastMinAnnotationsVideoQuery->toSql() . ') as temp'))
                ->mergeBindings($lastMinAnnotationsVideoQuery->getQuery())
                ->select(DB::raw('annotation_date, COUNT(*) as counter'))
                ->where('in_counter', '>=', config('vhd.min_annotation_video'))
                ->groupBy('annotation_date')
                ->orderBy('annotation_date', 'ASC')
                ->pluck('counter', 'annotation_date');
        return $minAnnotationsVideo;
    }

}
