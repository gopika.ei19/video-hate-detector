<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use App\YTVideoSnippet;
use App\Annotation;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $readyVideos = YTVideoSnippet::select('id')
                ->join('content_details', 'yt_video_snippets.id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->where('yt_video_snippets.ready', '=', 1)
                ->whereBetween('content_details.duration', config('vhd.duration_filter'))
                ->pluck('id')
                ->toArray();
        $readyVideosCounter = count($readyVideos);

        $annotationsMainQuery = Annotation::join('yt_video_snippets', 'annotations.video_id', '=', 'yt_video_snippets.id')
                ->join('content_details', 'annotations.video_id', '=', 'content_details.yt_video_snippets_id')
                ->where('yt_video_snippets.ready', '=', 1)
                ->where('yt_video_snippets.default_audio_language', 'like', 'pt%')
                ->whereBetween('content_details.duration', config('vhd.duration_filter'));
        $allPTAnnotationsQuery = clone $annotationsMainQuery;
        $allPTAnnotationsQuery->select(DB::raw('count(*) as annotations_counter, video_id'))
                ->groupBy('video_id');
        $videosPTFullyAnnotatedQuery = DB::table(DB::raw('(' . $allPTAnnotationsQuery->toSql() . ') as annotations_per_video'))
                ->mergeBindings($allPTAnnotationsQuery->getQuery())
                ->select('video_id')
                ->where('annotations_counter', '>=', config('vhd.min_annotation_video'));
        $videosFullyAnnotatedPT = $videosPTFullyAnnotatedQuery->pluck('video_id')->toArray();

        $annotationsChartData = self::mergeAnnotationsCounters(
                        AnnotationController::getAllAnnotationsPerDay(), AnnotationController::getMinAnnotationsVideoPerDay()
        );

        return view('home')->with([
                    'topAnnotators' => AnnotationController::getRanking(10),
                    'fullyAnnotatedVideos' => count($videosFullyAnnotatedPT),
                    'readyVideos' => $readyVideosCounter,
                    'annotationsByDay' => $annotationsChartData
        ]);
    }

    public static function mergeAnnotationsCounters($allAnnotationsCounter, $minReachedAnnotationsCounter) {
        $finalData = [];
        foreach ($allAnnotationsCounter as $day => $counter) {
            $finalData[$day] = [
                'all' => $counter,
                'videosFullyAnnotated' => isset($minReachedAnnotationsCounter[$day]) ? $minReachedAnnotationsCounter[$day] : 0
            ];
        }
        return $finalData;
    }

}
