<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\YTVideoSnippet;

class VideoController extends Controller {

    public function getAll(Request $request) {
        if (!(Auth::check() && Auth::user()->is_admin)) {
            abort(404);
        }

        if (!$request->expectsJson()) {
            return view('videos');
        }

        $result = new \stdClass();
        $result->draw = (int) $request->draw;

        $query = YTVideoSnippet::select(['id', 'title', 'default_audio_language', 'downloaded', 'ready', 'content_details.duration', 'content_details.caption'])
                ->where(function($q) {
            $q->where('default_audio_language', 'LIKE', 'en%')
            ->orWhere('default_audio_language', 'LIKE', 'pt%');
        });
        

        $durationIncluded = FALSE;
        foreach ($request->order as $order) {
            $columnName = $request->columns[(int) $order['column']]['data'];
            if ($columnName === 'duration') {
                $query->join('content_details', 'yt_video_snippets.id', '=', 'content_details.yt_video_snippets_id')
                        ->orderBy('content_details.' . $columnName, $order['dir']);
                $durationIncluded = TRUE;
            } else {
                $query->orderBy($columnName, $order['dir']);
            }
        }
        if (!$durationIncluded) {
            $query->join('content_details', 'yt_video_snippets.id', '=', 'content_details.yt_video_snippets_id');
        }
        $query->whereBetween('content_details.duration', config('vhd.duration_filter'));
        //Log::debug($query->toSql());
        $result->recordsTotal = $query->count();
        $result->recordsFiltered = $result->recordsTotal;

        $result->data = $query->offset($request->start)
                ->limit($request->length)
                ->get();
        foreach ($result->data as $video) {
            $video->title = json_decode($video->title);
            $video->duration = $video->contentDetail->duration;
        }

        return response()->json($result);
    }

    public function viewVideo(Request $request, $id) {
        $video = YTVideoSnippet::find($id);
        if($video === NULL){
            abort(404);
        }
        return view('video')->with([
                    'video' => $video
        ]);
    }

    public function updateVideo(Request $request, $id) {
        if (!(Auth::check() && Auth::user()->is_admin)) {
            abort(404);
        }
        $video = YTVideoSnippet::find($id);
        $video->ready = $request->has("ready");
        Log::info("Video $id was marked as " . ($video->ready ? "" : "NOT ") . "ready");

        $video->save();

        return redirect()->action("VideoController@getAll");
    }

}
