<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class YTVideoSnippet extends Model {

    protected $table = 'yt_video_snippets';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public $incrementing = FALSE;
    private $videoMetadata = FALSE;
    protected $fillable = ['id'];

    public function annotations() {
        return $this->hasMany('App\Annotation', 'video_id');
    }

    public function contentDetail() {
        return $this->hasOne('App\ContentDetail', 'yt_video_snippets_id');
    }

    /**
     * Lazy loads the video metadata from the file system.
     * @return array The metadata for the video as an object with 2 properties:
     * "url" and "type". In case no file is found in the file system, an empty
     * array is returned.
     */
    public function getMetadata() {
        if ($this->videoMetadata !== FALSE) {
            return $this->videoMetadata;
        }

        //Loads the video files metadata, which is not loaded yet.
        $fullPaths = $this->getFilesPaths();
        if (empty($fullPaths)) {
            $this->videoMetadata = [];
        } else {
            $metadata = [];
            foreach ($fullPaths as $fullPath) {
                $videoFilename = substr($fullPath, strlen(config('vhd.videos_folder')));
                $video = new \stdClass();
                $video->url = filter_input(INPUT_SERVER, 'REQUEST_SCHEME')
                        . '://' . filter_input(INPUT_SERVER, 'SERVER_NAME')
                        . '/videos/' . $videoFilename;
                $video->type = mime_content_type($fullPath);
                $metadata[] = $video;
            }
            $this->videoMetadata = $this->moveMP3ToEnd($metadata);
        }

        return $this->videoMetadata;
    }

    private function getFilesPaths() {
        $location = config('vhd.videos_folder') . $this->id . '*';
        $videos = glob($location);
        if (empty($videos)) {
            Log::error('No video found for ' . $location);
            return [];
        } else {
            return $videos;
        }
    }

    private function moveMP3ToEnd($metadata) {
        $files = [];
        $mp3 = '.mp3';
        $mp3File = NULL;
        foreach ($metadata as $file) {
            if (strtolower(substr($file->url, -(strlen($mp3)))) === $mp3) {
                $mp3File = $file;
            } else {
                $files[] = $file;
            }
        }
        if ($mp3File !== NULL) {
            $files[] = $mp3File;
        }
        return $files;
    }

}
