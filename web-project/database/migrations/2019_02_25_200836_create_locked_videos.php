<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLockedVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locked_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->char('yt_video_snippets_id', 11);
            $table->foreign('yt_video_snippets_id')
                    ->references('id')
                    ->on('yt_video_snippets')
                    ->onDelete('NO ACTION');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('CASCADE');
            $table->dateTime('expiration_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locked_videos');
    }
}
