<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYtVideoSnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yt_video_snippets', function (Blueprint $table) {
            $table->char('id', 11);
            $table->primary('id');
            $table->dateTime('published_at');
            $table->string('channel_id', 24);
            $table->string('title', 1000);
            $table->text('description', 10000);
            $table->string('tags', 2500)->nullable();
            $table->string('category_id', 45)->nullable();
            $table->string('live_broadcast_content', 8);
            $table->string('default_language', 10)->nullable();
            $table->string('default_audio_language', 10)->nullable();
            $table->boolean('available')->nullable();
            $table->boolean('downloaded')->default(FALSE);
            $table->dateTime('last_checked')->nullable();
            $table->boolean('ready')->nullable();
            //$table->integer('duration'); //TODO: move to content_details
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yt_video_snippets');
    }
}
