<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Essas credenciais não foram encontradas ou estão incorretas',
    'throttle' => 'Muitas tentativas de entrar. Por favor, tente novamente em :seconds segundos.',
    
    'login' => 'Entrar',
    'log_in' => 'entre',
    'remember_me' => 'Lembrar de Mim',
    'forgot_password' => 'Esqueceu sua senha?',
    'register' => 'Cadastrar',
    'register_yourself' => 'Cadastre-se',
    'login_alternative' => 'OU ENTRE USANDO',
    'failed_social_attempt' => 'Sua tentativa falhou. Tente novamente ou use um método diferente.',
    
    'name_field' => 'Nome',
    'email_field' => 'Endereço de E-Mail',
    'password_field' => 'Senha',
    'password_confirmation_field' => 'Confirmação de Senha',
    'recaptcha_unchecked' => 'Você tem que marcar o campo do recaptcha',

];
