<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senhas precisam ter pelo menos seis caracteres e precisam ser confirmadas corretamente.',
    'reset' => 'Sua senha foi redefinida!',
    'sent' => 'Nós te enviamos um email com o link de redefinição de senha!',
    'sent_log' => 'Um link para recuperar sua senha foi criado. Entre em contato com <a href="mailto:cleber.93cd@gmail.com">cleber.93cd@gmail.com</a> para obter o link.',
    'token' => 'Esse código de redefinição de senha é inválido.',
    'user' => 'Nós não conseguimos encontrar um usuário com esse endereço de email.',
    
    'reset_password' => 'Redefinir Senha',
    'send_password' => 'Enviar Link de Redefinição de Senha',
];
