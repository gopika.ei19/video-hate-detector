<?php

return [
    '404' => 'Página Não Encontrada',
    '500' => 'Erro Interno do Servidor',
];
