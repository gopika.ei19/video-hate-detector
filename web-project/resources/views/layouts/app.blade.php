<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('partials.htmlheader')
    <body>
        @include('partials.navbar')

        @yield('main-content')

        @include('partials.footer')

        @include('partials.scripts')
    </body>
</html>
