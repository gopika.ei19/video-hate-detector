@extends('layouts.contentpage')

@section('title')
{{ucwords(mb_strtolower(trans('general.ranking')))}}
@endsection

@section('content')
<div class="container mtb">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h4><i class='fa fa-list-ol'></i> {{ucwords(mb_strtolower(trans('general.ranking')))}}</h4>
                    </div>
                </div>
            </div>
            <div class="hline"></div>
            @if(count($topAnnotators) === 0)
            <br/>
            <div class="alert alert-info col-lg-6 col-lg-offset-3" role="alert">
                <i class='fa fa-info-circle'></i> <strong>{{trans('general.no_annotators')}}</strong><br/>
                <div class="text-center">
                    <i class='fa fa-frown-o fa-5x'></i>
                </div>
            </div>
            @else
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th class='text-center'>{{trans('general.top_annotators_rank')}}</th>
                        <th>{{trans('general.top_annotators_name')}}</th>
                        <th class='text-center' data-toggle="tooltip" data-placement="bottom" title="{{trans('general.top_annotators_amount_tooltip')}}">#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($topAnnotators as $annotator)
                    <tr class='{{$loop->index < 3 ? "bg-success":""}}'>
                        <td class='text-center'>{{$loop->iteration}}º</td>
                        <td>
                            @if(!Auth::guest() && Auth::user()->is_admin)
                            <a href="{{route('user_annotations', ['userId' => $annotator['id']])}}">{{$annotator['name']}}</a>
                            @else
                            {{explode(' ', $annotator['name'])[0]}}
                            @endif
                            @if(!Auth::guest() && $annotator['id'] === Auth::user()->id)
                            <span class='label label-success'>{{trans('general.you_indicator')}}</span>
                            @endif
                        </td>
                        <td class='text-center'>
                            <span class='label {{$loop->index < 3 ? "label-success":"label-default"}}'>{{$annotator['annotations_count']}}</span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
@endsection