@extends('layouts.app')

@section('htmlheader_title')
{{trans('errors.404')}}
@endsection

@section('main-content')
    <div class="container mtb text-warning text-center">
        <div class="row">
            <div class="col-lg-12">
                <div class="mtb hidden-xs"></div>
                <h1 style="font-size: 10em"><i class="fa fa-exclamation-triangle"></i> 404</h1>
                <div class="mtb hidden-xs"></div>
                <h1>{{trans('errors.404')}}</h1>
            </div>
        </div>
    </div>
@endsection