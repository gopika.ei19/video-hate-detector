@extends('layouts.app')

@section('htmlheader_title')
{{trans('errors.500')}}
@endsection

@section('main-content')
    <div class="container mtb text-danger text-center">
        <div class="row">
            <div class="col-lg-12">
                <div class="mtb hidden-xs"></div>
                <h1 style="font-size: 10em"><i class="fa fa-exclamation-triangle"></i> 500</h1>
                <div class="mtb hidden-xs"></div>
                <h1>{{trans('errors.500')}}</h1>
            </div>
        </div>
    </div>
@endsection