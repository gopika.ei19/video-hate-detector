<div class="modal fade" id="instructionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">{{trans('general.instructions')}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info" role="alert">
                            <i class='fa fa-star'></i><strong> Olá, {{ Auth::user()->firstName() }}!</strong><br/>
                            <p>Você irá nos ajudar a treinar um sistema de reconhecimento de vídeos ofensivos. Antes de iniciar, leia as instruções abaixo.</p>
                        </div>

                        <h4>Definição da Tarefa</h4>
                        {{-- {{trans('general.instructions_text')}} --}}
                        <p>Sua tarefa é assistir aos vídeos que serão apresentados e indicar se eles contêm algum trecho ofensivo baseando-se <strong>apenas no que as pessoas falam no vídeo</strong>. Por ofensivo, considere as seguintes categorias e suas definições:</p>
                        - <strong>Racismo</strong>:
                        Preconceito, discriminação ou antagonismo direcionado contra alguém de uma raça diferente baseado na crença de que a raça de alguém é superior.
                        <a href="https://en.oxforddictionaries.com/definition/racism" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                        - <strong>Sexismo</strong>:
                        Atitude, discurso ou comportamento, que se baseia no preconceito e na discriminação sexual: a exaltação exagerada do masculino ou do feminino é uma forma de sexismo.
                        <a href="https://en.oxforddictionaries.com/definition/sexism" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                        - <strong>Homofobia</strong>:
                        Desgosto ou preconceito contra pessoas homossexuais.
                        <a href="https://en.oxforddictionaries.com/definition/homophobia" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                        - <strong>Xenofobia</strong>:
                        Desgosto ou preconceito contra pessoas de outras localidades, seja no contexto de uma comunidade local, estado ou país.
                        <a href="https://en.oxforddictionaries.com/definition/xenophobia" target="_blank"><i class='fa fa-external-link'></i></a>
                        <a href="http://www.unesco.org/new/en/social-and-human-sciences/themes/international-migration/glossary/xenophobia/" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                        - <strong>Intolerância Religiosa</strong>:
                        Preconceiro ou desrespeito pela crença religiosa de outras pessoas.
                        <a href="https://en.oxforddictionaries.com/definition/intolerance" target="_blank"><i class='fa fa-external-link'></i></a>
                        <a href="http://www.religioustolerance.org/relintol1.htm" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                        - <strong>Linguagem Obscena</strong>:
                        Uso de palavrões ou xingamentos.
                        <a href="https://en.oxforddictionaries.com/definition/profanity" target="_blank"><i class='fa fa-external-link'></i></a>
                        </br>
                   </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>Execução da Tarefa</h4>
                        <p>Execute as seguintes ações durante a avaliação do vídeo:</p>
                        1. Clique no botão "{!!trans('general.player_play')!!}" e execute o vídeo.<br/>
                        2. A cada vez que algo ofensivo for dito no vídeo, clique no botão "{!!trans('general.tag_moment')!!}".</br>
                        3. Ao final da execução do vídeo, caso nada ofensivo tenha sido dito, clique no botão "{{trans('general.not_offensive')}}".</br>
                        4. Aperte o botão "{{trans('general.save')}}".</br>
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-warning" role="alert">
                            <i class='fa fa-exclamation-triangle'></i><strong> Tempo de avaliação</strong><br/>
                            <p>Cada vídeo ficará disponível para avaliação por {{config('vhd.video_lock_factor')}} vezes a duração do vídeo.
                                Avalie o vídeo dentro deste tempo. Caso contrário, a página será recarregada e a avaliação atual será perdida.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{trans('general.ok')}}</button>
            </div>
        </div>
    </div>
</div>