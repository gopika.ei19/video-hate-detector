<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">{{trans('general.toggle_navigation')}}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{Helpers::url('')}}">{{config('app.name')}}</a>
        </div>
        <div class="navbar-collapse collapse navbar-left">
            <ul class="nav navbar-nav">
                <li class="dropdown {{Helpers::request_is(['annotations', 'annotations/ranking']) ? 'active' : '' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.annotations')}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="{{ Helpers::request_is('annotations') ? 'active' : '' }}">
                            <a href="{{Helpers::url('annotations')}}">{{trans('general.my_annotations')}}</a>
                        </li>
                        @if(Auth::check())
                        <li class="{{ Helpers::request_is('annotations/ranking') ? 'active' : '' }}">
                            <a href="{{Helpers::url('annotations/ranking')}}">{{trans('general.ranking')}}</a>
                        </li>
                        @endif
                    </ul>
                </li>
                @if(Auth::check() && Auth::user()->is_admin)
                <li class="dropdown {{Helpers::request_is(['videos', 'statistics']) ? 'active' : '' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.admin')}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="{{ Helpers::request_is('videos') ? 'active' : '' }}">
                            <a href="{{Helpers::url('videos')}}">{{trans('general.videos')}}</a>
                        </li>
                        <li class="{{ Helpers::request_is('statistics') ? 'active' : '' }}">
                            <a href="{{Helpers::url('statistics')}}">{{trans('general.statistics')}}</a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                <li class="{{ Helpers::request_is('login') ? 'active' : '' }}">
                    <a href="{{Helpers::url('login')}}">{{mb_strtoupper(trans('auth.login'))}}</a>
                </li>
                <li class="{{ Helpers::request_is('register') ? 'active' : '' }}">
                    <a href="{{Helpers::url('register')}}">{{mb_strtoupper(trans('auth.register'))}}</a>
                </li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->firstName() }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li class="{{ Helpers::request_is('logout') ? 'active' : '' }}">
                            <a href="{{Helpers::url('logout')}}"
                               onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endif
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <img src="{{asset('img/flags/blank.gif')}}" class="flag flag-{{config('app.available_locales')[app()->getLocale()]['flag_id']}}" alt="{{config('app.available_locales')[app()->getLocale()]['flag_name']}}" />
                        {{ strtoupper(app()->getLocale()) }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @foreach(config("app.available_locales") as $localeKey => $localeValue)
                        <li class="{{ app()->getLocale() === $localeKey ? 'active' : '' }}">
                            <a href="{{Helpers::get_current_url_with_locale($localeKey)}}">
                                <img src="{{asset('img/flags/blank.gif')}}" class="flag flag-{{$localeValue['flag_id']}}" alt="{{$localeValue['flag_name']}}" /> {{$localeValue['language']}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
