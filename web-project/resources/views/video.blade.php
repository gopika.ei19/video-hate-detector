@extends('layouts.contentpage')

@section('title')
{{trans('general.view_video')}}
@endsection

@section('content')
<div class="container mtb">
@if(empty($video->getMetadata()))
    <div class='row'>
        <div class="col-lg-12">
            <h4>
                <i class='fa fa-film'></i>
                {{ucwords(trans_choice('general.video',1)) . ': '}}
                <a target="_blank" href="{{url('https://youtube.com/watch?v=' . $video->id)}}">
                    {{$video->id}}
                    <i class="fa fa-external-link"></i>
                </a>
            </h4>
            <div class="hline"></div>
            <p></p>
            <div class="alert alert-info" role="alert">
                <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.no_video_available')}}</strong><br/>
                {{trans('general.no_video_available_text')}}
                <div class="text-center hidden-xs" style="font-size: 100pt">
                    <i class='fa fa-film'></i>
                    <i class='fa fa-clock-o'></i>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <h4>
                <i class='fa fa-film'></i>
                {{ucwords(trans_choice('general.video',1)) . ': '}}
                <a target="_blank" href="{{url('https://youtube.com/watch?v=' . $video->id)}}">
                    {{$video->id}}
                    <i class="fa fa-external-link"></i>
                </a>
            </h4>
            <div class="hline"></div>
            <p></p>
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <video class="img-responsive center-block" controls style="width: 100%;">
                    @foreach($video->getMetadata() as $metadata)
                    <source src="{{$metadata->url}}" type="{{$metadata->type}}">
                    @endforeach
                    <p>Your browser doesn't support HTML5 video.</p>
                </video>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <h4><i class='fa fa-gear'></i> {{trans('general.settings')}}</h4>
            <div class='hline'></div>
            <p></p>
            <form method='POST'>
                {{csrf_field()}}
                <div class='form-group'>
                    <label for='ready'>{!!trans('general.video_ready')!!}</label>
                    @if($video->ready === NULL)
                    <span class='label label-warning pull-right'>
                        <i class='fa fa-exclamation-triangle'></i> {{trans('general.not_defined')}}
                    </span>
                    @endif
                    <input type="checkbox"
                           name="ready"
                           data-toggle="toggle" 
                           data-on='<i class="fa fa-check"></i>'
                           data-off='<i class="fa fa-times"></i>'
                           data-onstyle='success'
                           @if($video->ready !== NULL)
                           data-offstyle='danger'
                           @endif
                           data-width='100%' {{$video->ready? 'checked':'NOT'}}>
                </div>
                <button type='submit' class='btn btn-theme btn-block' style='margin: 0px;'><i class='fa fa-save'></i> {{trans('general.save_settings')}}</button>
            </form>
        </div>
    </div>
@endif
</div>
@endsection

@section('page-scripts')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection