#!/usr/bin/python


'''
This script is responsible for managing database operations.
'''
import sys
import os
import mysql.connector
import json
import dateutil.parser
import datetime

if not os.path.abspath('../') in sys.path:
	sys.path.append(os.path.abspath('../'))


import vhd_utils as util


util.LOG_LEVEL = util.LOG_INFO


def get_connector():
	'''
	Gets the database connector
	'''
	mydb = mysql.connector.connect(
	  host='localhost',
	  user='root',
	  passwd='password123',
	  database='vhd'
	)

	return mydb

def perform_insert(query, insert_tuple):
	'''
	Inserts a general tuple based on a SQL query
	'''
	mydb = get_connector()
	cursor = mydb.cursor()
	try:
		util.log_debug('SQL Query: ' + query)
		util.log_debug('Tuple: ' + str(insert_tuple))
		cursor.execute(query, insert_tuple)
		mydb.commit()
		util.log_debug(str(cursor.rowcount) + ' record(s) inserted.')
		if(cursor.rowcount == 1):
			return {
				'status': 0,
				'message': '',
				'error': None
			}
		else:
			return {
				'status': 2,
				'message': str(cursor.rowcount) + ' inserted.'
				,
				'error': None
			}
	except Exception as e:
		util.log_error('Error for video ' + insert_tuple[0] + ': ' + str(e))
		return {
			'status': 3,
			'message': str(e),
			#'error': e
		}

def perform_update(query, update_tuple):
	'''
	Updates a record in the database based on a SQL query
	'''
	mydb = get_connector()
	cursor = mydb.cursor()
	try:
		util.log_debug('SQL Query: ' + query)
		util.log_debug('Tuple: ' + str(update_tuple))
		cursor.execute(query, update_tuple)
		mydb.commit()
		util.log_debug(str(cursor.rowcount) + ' record(s) updated.')
		if(cursor.rowcount == 1):
			return {
				'status': 0,
				'message': '',
				'error': None
			}
		else:
			return {
				'status': 2,
				'message': str(cursor.rowcount) + ' updated.'
				,
				'error': None
			}
	except Exception as e:
		util.log_error('Error for video ' + update_tuple[0] + ': ' + str(e))
		return {
			'status': 3,
			'message': str(e),
			#'error': e
		}
