#!/usr/bin/python


'''
This script is responsible for managing database operations.
'''
import sys
import os
import mysql.connector
import json
import dateutil.parser
import datetime

if not os.path.abspath('../') in sys.path:
  sys.path.append(os.path.abspath('../'))

import utils as db_util
import vhd_utils as util


util.LOG_LEVEL = util.LOG_INFO


def insert_video(video):
  '''
  Inserts a video in the database and returns a result array.
  '''
  util.log_debug('Inserting video ' + video['id'] + ' in the database...')
  
  errors = []

  result = insert_snippet(video)
  if result['status'] > 1:
    errors.append(result)

  result = insert_statistics(video)
  if result['status'] > 1:
    errors.append(result)

  result = insert_thumbnails(video)
  if result['status'] > 1:
    errors.append(result)

  result = insert_localized(video)
  if result['status'] > 1:
    errors.append(result)

  result = insert_content_details(video)
  if result['status'] > 1:
    errors.append(result)

  if len(errors) > 0:
    status = 1
  else:
    status = 0

  return {
    'id': video['id'],
    'status': status,
    'errors': errors
  }


def insert_snippet(video):
  '''
  Inserts a video snippet in the database
  '''
  mydb = db_util.get_connector()
  cursor = mydb.cursor()

  sql = 'SELECT id FROM yt_video_snippets WHERE id = "%s"' % video['id']
  cursor.execute(sql)
  if len(cursor.fetchall()) > 0:
    return {
      'status': 1,
      'message': 'Snippet already exists',
      'error': None
    }
  
  sql = ('INSERT INTO yt_video_snippets ('
    + 'id, '
    + 'published_at, '
    + 'channel_id, '
    + 'title, '
    + 'description, '
    + 'tags, '
    + 'category_id, '
    + 'live_broadcast_content, '
    + 'default_language, '
    + 'default_audio_language, '
    + 'available, '
    + 'downloaded, '
    + 'last_checked, '
    + 'ready'
    + ') VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)')
  
  try:
    video_tuple = get_video_snippet_tuple(video)
    result = db_util.perform_insert(sql, video_tuple)
  except Exception as e:
    util.log_error('Error: ' + str(e))
    result = {
      'status': 3,
      'message': str(e)
    }

  return result

def get_video_snippet_tuple(video):
  '''
  Creates a video statistics tuple based in a video object
  '''
  # Published at
  publishedAt = dateutil.parser.parse(video['snippet']['publishedAt'])

  # Tags
  if('tags' in video['snippet']):
    tags = json.dumps(video['snippet']['tags'])
  else:
    tags = json.dumps([])

  # Default language
  if('defaultLanguage' in video['snippet']):
    defaultLanguage = video['snippet']['defaultLanguage']
  else:
    defaultLanguage = None

  # Default audio language
  if('defaultAudioLanguage' in video['snippet']):
    defaultAudioLanguage = video['snippet']['defaultAudioLanguage']
  else:
    defaultAudioLanguage = None

  video_snippet_tuple = (
    video['id'],
    publishedAt,
    video['snippet']['channelId'],
    json.dumps(video['snippet']['title']),
    json.dumps(video['snippet']['description']),
    tags,
    video['snippet']['categoryId'],
    video['snippet']['liveBroadcastContent'],
    defaultLanguage,
    defaultAudioLanguage,
    None,
    False,
    None,
    None
    )

  return video_snippet_tuple


def insert_statistics(video):
  '''
  Inserts the statistics of a video in the database
  '''
  mydb = db_util.get_connector()
  cursor = mydb.cursor()

  sql = 'SELECT yt_video_snippets_id FROM statistics WHERE yt_video_snippets_id = "%s"' % video['id']
  cursor.execute(sql)
  if len(cursor.fetchall()) > 0:
    return {
      'status': 1,
      'message': 'Statistics already exist',
      'error': None
    }
  
  sql = ('INSERT INTO statistics ('
    + 'yt_video_snippets_id, '
    + 'view_count, '
    + 'like_count, '
    + 'dislike_count, '
    + 'favorite_count, '
    + 'comment_count, '
    + 'last_checked'
    + ') VALUES (%s, %s, %s, %s, %s, %s, %s)')
  
  try:
    video_tuple = get_video_statistics_tuple(video)
    result = db_util.perform_insert(sql, video_tuple)
  except Exception as e:
    util.log_error('Error: ' + str(e))
    result = {
      'status': 3,
      'message': str(e)
    }

  return result

def get_video_statistics_tuple(video):
  '''
  Creates a video statistics tuple based in a video object
  '''
  # View count
  if('viewCount' in video['statistics']):
    viewCount = video['statistics']['viewCount']
  else:
    viewCount = None

  # Like count
  if('likeCount' in video['statistics']):
    likeCount = video['statistics']['likeCount']
  else:
    likeCount = None

  # Dislike count
  if('dislikeCount' in video['statistics']):
    dislikeCount = video['statistics']['dislikeCount']
  else:
    dislikeCount = None

  # Favorite count
  if('favoriteCount' in video['statistics']):
    favoriteCount = video['statistics']['favoriteCount']
  else:
    favoriteCount = None

  # Comment count
  if('commentCount' in video['statistics']):
    commentCount = video['statistics']['commentCount']
  else:
    commentCount = None

  # Last checked. Default value is when the video was posted. This value should be updated later.
  lastChecked = dateutil.parser.parse(video['snippet']['publishedAt'])

  video_tuple = (
    video['id'],
    viewCount,
    likeCount,
    dislikeCount,
    favoriteCount,
    commentCount,
    lastChecked
    )

  return video_tuple 


def insert_thumbnails(video):
  '''
  Inserts the thumbnails of a video in the database
  '''
  mydb = db_util.get_connector()
  cursor = mydb.cursor()

  counter = len(video['snippet']['thumbnails'])

  sql = 'SELECT thumbnail_key FROM thumbnails WHERE yt_video_snippets_id = "%s"' % video['id']
  cursor.execute(sql)
  if len(cursor.fetchall()) == counter:
    return {
      'status': 1,
      'message': 'Thmbnails already exist',
      'error': None
    }
  
  sql = ('INSERT INTO thumbnails ('
    + 'yt_video_snippets_id, '
    + 'thumbnail_key, '
    + 'url, '
    + 'width, '
    + 'height'
    + ') VALUES (%s, %s, %s, %s, %s)')
  
  results = []
  highest_status = 0;
  for thumbnail_key in video['snippet']['thumbnails']:
    try: 
      video_tuple = get_video_thumbnail_tuple(video, thumbnail_key)
      result = db_util.perform_insert(sql, video_tuple)
      if(result['status'] > highest_status):
        highest_status = result['status']
    except Exception as e:
      util.log_error('Error: ' + str(e))
      result = {
        'status': 3,
        'message': str(e)
      }
    results.append(result)
  if highest_status > 0:
    message = 'Some error occurred'
  else:
    message = ''

  return {
    'status': highest_status,
    'message': message,
    'errors': results
  }

def get_video_thumbnail_tuple(video, thumbnail_key):
  '''
  Creates a video thumbnail tuple based in a video object
  '''
  video_tuple = (
    video['id'],
    thumbnail_key,
    video['snippet']['thumbnails'][thumbnail_key]['url'],
    video['snippet']['thumbnails'][thumbnail_key]['width'],
    video['snippet']['thumbnails'][thumbnail_key]['height']
    )

  return video_tuple 


def insert_localized(video):
  '''
  Inserts the localized information of a video in the database
  '''
  mydb = db_util.get_connector()
  cursor = mydb.cursor()

  sql = 'SELECT yt_video_snippets_id FROM localized WHERE yt_video_snippets_id = "%s"' % video['id']
  cursor.execute(sql)
  if len(cursor.fetchall()) > 0:
    return {
      'status': 1,
      'message': 'Localized already exist',
      'error': None
    }
  
  sql = ('INSERT INTO localized ('
    + 'yt_video_snippets_id, '
    + 'title, '
    + 'description'
    + ') VALUES (%s, %s, %s)')
  
  try:
    video_tuple = get_video_localized_tuple(video)
    result = db_util.perform_insert(sql, video_tuple)
  except Exception as e:
    util.log_error('Error: ' + str(e))
    result = {
      'status': 3,
      'message': str(e)
    }

  return result

def get_video_localized_tuple(video):
  '''
  Creates a video localized tuple based in a video object
  '''
  video_tuple = (
    video['id'],
    json.dumps(video['snippet']['localized']['title']),
    json.dumps(video['snippet']['localized']['description'])
    )

  return video_tuple


def insert_content_details(video):
  '''
  Inserts the content details information of a video in the database
  '''
  mydb = db_util.get_connector()
  cursor = mydb.cursor()

  sql = 'SELECT yt_video_snippets_id FROM content_details WHERE yt_video_snippets_id = "%s"' % video['id']
  cursor.execute(sql)
  if len(cursor.fetchall()) > 0:
    return {
      'status': 1,
      'message': 'Content details already exist',
      'error': None
    }
  
  sql = ('INSERT INTO content_details ('
    + 'yt_video_snippets_id, '
    + 'duration, '
    + 'dimension, '
    + 'definition, '
    + 'caption, '
    + 'licensed_content, '
    + 'region_restricted, '
    + 'projection, '
    + 'has_custom_thumbnail'
    + ') VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)')
  
  try:
    video_tuple = get_video_content_details_tuple(video)
    result = db_util.perform_insert(sql, video_tuple)
  except Exception as e:
    util.log_error('Error: ' + str(e))
    result = {
      'status': 3,
      'message': str(e)
    }

  return result

def get_video_content_details_tuple(video):
  '''
  Creates a video content details tuple based in a video object
  '''

  duration = util.convert_duration(video['contentDetails']['duration'])
  
  # Region restricted
  if('regionRestricted' in video['contentDetails']):
    regionRestricted = video['contentDetails']['regionRestricted']
  else:
    regionRestricted = None

  # Has custom thumbnail
  if('hasCustomThumbnail' in video['contentDetails']):
    hasCustomThumbnail = bool(video['contentDetails']['hasCustomThumbnail'])
  else:
    hasCustomThumbnail = None

  video_tuple = (
    video['id'],
    duration,
    video['contentDetails']['dimension'],
    video['contentDetails']['definition'],
    bool(video['contentDetails']['caption']),
    bool(video['contentDetails']['licensedContent']),
    regionRestricted,
    video['contentDetails']['projection'],
    hasCustomThumbnail
    )

  return video_tuple


def filter_errors(videos, errors):
  '''
  Filters the videos with errors from the videos list so only the ones with errors are processed.
  '''
  errors_keys = list(map(lambda error: error['id'], errors))
  util.log_info('Videos with errors from previous import: ' + str(len(errors_keys)))
  new_videos = list(filter(lambda video: video['id'] in errors_keys, videos))
  util.log_info('Videos for this import (after filtering): ' + str(len(new_videos)))
  return new_videos


if __name__ == '__main__':
  startTime = datetime.datetime.now()
  util.log_info('Start time: ' + str(startTime))

  if len(sys.argv) < 2:
    print 'ERROR: You must provide an input file. The file should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python import.py input.json\n'
  else:
    input_file = sys.argv[1]

    videos = util.load_json(input_file)
    # Use the lines below to retry to import only videos with errors in the previous import.
    #errors = util.load_json('../import_errors.json')
    #videos = filter_errors(videos, errors)

    filteredVideos = util.filter_defaultAudioLanguage_PT_EN(videos)
    total = len(filteredVideos)
    counter = 1
    video_errors = []
    for video in videos:
      util.log_info('Processing video %d out of %d...' % (counter, total))
      counter += 1
      result = insert_video(video)
      if result['status'] > 0:
        video_errors.append(result)

    util.log_info('Errors: ' + json.dumps(video_errors))

  endTime = datetime.datetime.now()
  util.log_info('End time: ' + str(endTime))
  util.log_info('Elapsed time: ' + str(endTime - startTime))
