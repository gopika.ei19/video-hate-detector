#!/usr/bin/python


'''
This script is responsible for updating the downloaded status of the videos in the database.
'''
import sys
import os
import glob
import mysql.connector
import json
import dateutil.parser
import datetime

if not os.path.abspath('../') in sys.path:
	sys.path.append(os.path.abspath('../'))

import utils as db_util
import vhd_utils as util


util.LOG_LEVEL = util.LOG_INFO


def get_videos_list():
	'''
	Gets the list of videos in the database
	'''
	mydb = db_util.get_connector()
	cursor = mydb.cursor()

	sql = 'SELECT yt_video_snippets_id, caption FROM content_details'
	cursor.execute(sql)
	videos_ids = cursor.fetchall()
	return videos_ids

def update_subtitle_status(status_tuples):
	'''
	Updates the subtitle status for a set of videos in the database.
	'''
	mydb = db_util.get_connector()
	cursor = mydb.cursor()

	sql = ('UPDATE content_details '
		+ 'SET caption = %s '
		+ 'WHERE yt_video_snippets_id = %s')
	
	results = []
	for status_tuple in status_tuples:
		try:
			results.append(db_util.perform_update(sql, status_tuple))
		except Exception as e:
			util.log_error('Error on update_subtitle_status: ' + str(e))
			results.append({
				'status': 3,
				'message': str(e)
			})

	return results


if __name__ == '__main__':
	startTime = datetime.datetime.now()
	util.log_info('Start time: ' + str(startTime))

	if len(sys.argv) < 2:
		print 'ERROR: You must provide the folder where the downloaded subtitles are.'
		print '\nUsage:'
		print '  python update_subtitle_status.py subtitles_folder\n'
	else:
		folder = sys.argv[1]
		videos_ids = get_videos_list()
		download_status = []
		total = len(videos_ids)
		counter = 1
		for video_id in videos_ids:
			util.log_info("Processing %d out of %d videos... (%.2f%%)" % (counter, total, (float(counter)*100)/total))
			counter+=1
			subtitles_found = len(glob.glob(folder + video_id[0] + '*'))
			if(subtitles_found > 0):
				util.log_debug("subtitles found for %s: %d" % (video_id[0], subtitles_found))
			else:
				util.log_debug("=== NO subtitles found for %s ===" % (video_id[0]))
			download_status.append((subtitles_found > 0, video_id[0]))

		util.save_json_list(download_status, 'hi.log')
		results = update_subtitle_status(download_status)
		util.save_json_list(results, 'update_subtitle_status_results.json')

	endTime = datetime.datetime.now()
	util.log_info('End time: ' + str(endTime))
	util.log_info('Elapsed time: ' + str(endTime - startTime))
