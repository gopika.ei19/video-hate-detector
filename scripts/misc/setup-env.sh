#!/bin/bash

# This script sets up the environment to run the python scripts.

sudo apt-get install -y python-pip

sudo -H pip install --upgrade pip

#https://developers.google.com/api-client-library/python/
sudo pip install --upgrade google-api-python-client google-auth google-auth-oauthlib google-auth-httplib2 flask requests

#https://rg3.github.io/youtube-dl/
sudo pip install --upgrade youtube_dl
sudo apt-get install -y libav-tools

sudo pip install mysql-connector

sudo pip install isodate python-dateutil
