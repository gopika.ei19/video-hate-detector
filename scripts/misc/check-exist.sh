#!/bin/bash

# This script checks if a specific list of videos are present in a specific 
# folder of the hard drive. Then, this script creates a sendond json with a list
# excluding the videos that were already downloaded.
# This was used to verify which videos downloaded in the laboratory were already
# downloaded on my personal computer.

# Folder to be verified.
VPATH="/media/cleber/Data/masters/videos/"
# Full list of videos to be checked
VFILE="videos-info-audio_2nd-half.json"
# List fo downloaded videos in the format of the output of a 'ls' command of the
# folder with video and audio files.
DOWNLOADED="downloaded-lab.log"

echo 'Starting...'
videos="\("
found=0
# Identifies the videos on this computer from the downloaded list
while read p; do
	if [ -f $VPATH$p ]; then
		echo "File $VPATH$p exists"
		videos=${videos}${p:0:11}"\|"
		found=$((found+1))
	fi
done < $DOWNLOADED
videos=${videos:0:-2}"\)"
echo $videos
echo "Videos found on this computer: "$found

lines_original=`cat $VFILE | wc -l`
echo "Lines of original file: " $lines_original

# Deletes the videos found from the "to download" list
sed "/$videos/d" $VFILE > tmp_second.json
lines_result=`cat tmp_second.json | wc -l`
echo "Lines of result file: " $lines_result

echo 'Done.'
