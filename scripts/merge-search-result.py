#!/usr/bin/python

'''
This script merges the results of the search-videos.py in case that script is run more than once and the output is sent to different files.
Usage:
python merge-search-result.py output.json input1.json input2.json ...
'''

import datetime
import sys
from vhd_utils import *

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime
  if len(sys.argv) < 4:
    print 'ERROR: You must provide 1 output file and at least 2 input files in order to perform the merge. The files should be an JSON list of strings.'
    print '\nUsage:'
    print '  python merge-search-result.py output.json input1.json input2.json ...\n'
  else:
    print 'Loading videos...'
    videos_lists = []
    for index in range(2, len(sys.argv)):
      videos_lists.append(load_json(sys.argv[index]))
    
    print 'Merging videos lists...'
    sum_list = []
    for videos in videos_lists:
      sum_list = sum_list + videos
    merged_list = set(sum_list)

    print 'Total videos: %d (%d duplicate(s) removed)' % (len(merged_list), len(sum_list) - len(merged_list))

    save_json_list(merged_list, sys.argv[1])

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime)
