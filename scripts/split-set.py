#!/usr/bin/python

'''
This script splits a set of videos in n parts ans save them to different files.
'''
import math
import datetime
import sys
from vhd_utils import *

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 3:
    print 'ERROR: You must provide an input file and the amount of parts to split it in. The file should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python split-set.py input.json parts\n'
  else:
    videos = load_json(sys.argv[1])
    parts = int(sys.argv[2])
    portion_size = int(math.ceil(len(videos)/float(parts)))
    print 'Splitting %d in %d parts (%d sized)...' % (len(videos), parts, portion_size)
    for i in range(parts):
      print 'Part %d...' % (i+1)
      new_set = videos[i*portion_size:(i+1)*portion_size]
      save_json_list(new_set, sys.argv[1] + str(i))


  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime) 

