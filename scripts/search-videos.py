#!/usr/bin/python

'''
This script seaarches for videos on YouTube using YouTube API and returns the 
results to a file.
'''

import json
import datetime
from vhd_utils import *

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = 'AIzaSyCnWqwBsfVEPaeMsgkdRF1rLkXDT9FrNEA'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

# Amount of videos to be retrieved at time
CHUNK_SIZE = 50
# Maximum amount of videos to be retrieved
MAX_RESULTS = 10000
# File containing the words to be used to search for the videos.
CURSES_FILE = 'datasources/curses.json'
# Output file for the found videos.
OUTPUT_FILE = 'datasources/videos-keys.json'


def youtube_search(keyword):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)
  nextPageToken = None
  totalResults = 0
  
  videos = []
  
  # Call the search.list method to retrieve results matching the specified
  # query term.
  while nextPageToken != '' and totalResults <= MAX_RESULTS:
    search_response = youtube.search().list(
      q=keyword,
      part='id',
      publishedAfter='2018-07-02T00:00:00Z', # Use this attribute as a filter
      maxResults=CHUNK_SIZE,
      pageToken=nextPageToken
    ).execute()
    
    if search_response.get('nextPageToken') is None:
      print 'There are no more pages.'
      nextPageToken = ''
    else:
      nextPageToken = search_response.get('nextPageToken')

    totalResults += len(search_response.get('items', []))
    print "Retrieved: %d items (%d total)" % (len(search_response.get('items', [])), totalResults)

    for search_result in search_response.get('items', []):
      if search_result['id']['kind'] == 'youtube#video':
        videos.append(search_result['id']['videoId'])
  return videos


if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime
  
  curses = load_json(CURSES_FILE)

  with open(OUTPUT_FILE, 'w') as outfile:
    outfile.write('[')

  try:
    last_line = ''
    for language, languageCurses in curses.iteritems():
      for curse in languageCurses:
        print 'Searching results for word "%s"' % curse
        videos = youtube_search(curse)
        print 'Saving results found for word "%s"' % curse
        with open(OUTPUT_FILE, 'a') as outfile:
          outfile.write(last_line + '"' + '",\n"'.join(videos) + '"')
        last_line=',\n'
  except HttpError, e:
    print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
  except Exception as e:
    print 'An error happened: ' + str(e)

  with open(OUTPUT_FILE, 'a') as outfile:
    outfile.write(']')

  remove_duplicates(OUTPUT_FILE)

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime)
