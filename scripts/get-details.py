#!/usr/bin/python

# This script downloads the metadata for a list of videos using YouTube API.

import argparse
import json
import csv
import math
import datetime
from vhd_utils import *

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = 'AIzaSyCnWqwBsfVEPaeMsgkdRF1rLkXDT9FrNEA'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
# Amount of videos to be retrieved at time
CHUNK_SIZE = 50
# File containing the videos list. This file is the output of the "search-videos.py"
VIDEOS_FILE = 'datasources/videos-keys-all-20180909.json'
# File to store the full info about the videos
OUTPUT_FILE = 'datasources/videos-details-all.json'

def get_videos_details(videos_ids_list):
  '''
  Gets the information of multiple videos from YouTube.
  '''
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)
  videos = []
  total = len(videos_ids_list)
  for i in range(int(math.ceil(total/float(CHUNK_SIZE)))):
    list_response = youtube.videos().list(
      id=','.join(videos_ids_list[CHUNK_SIZE*i:CHUNK_SIZE*(i+1)]),
      part='id,snippet,contentDetails,statistics,recordingDetails,localizations'
    ).execute()
    
    videos += list_response.get('items', [])
    print 'Video details retrieved for %d video(s) out of %d (%d to %d)' % (len(videos), total, CHUNK_SIZE*i, min(CHUNK_SIZE*(i+1), total))
  print 'Information retrieved for %d videos.' % len(videos)
  retrieved_videos_keys = map(lambda video_details: video_details['id'], videos)
  failed_videos_keys = list(set(videos_ids_list) - set(retrieved_videos_keys))
  print '%d videos failed to retrieve the details: %s' % (len(failed_videos_keys), str(failed_videos_keys))
  return videos

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime
  videos = load_json(VIDEOS_FILE)
  current_details = load_json(OUTPUT_FILE)

  print 'Excluding videos which already have the details downloaded...'
  current_info_keys = map(lambda video_details: video_details['id'], current_details)
  pending_info_keys = list(set(videos) - set(current_info_keys));
  
  try:
    print 'Getting full information of %d YouTube videos.' % len(pending_info_keys)

    videos_details = get_videos_details(pending_info_keys)
    all_videos_details = current_details + videos_details
    save_json_list(all_videos_details, OUTPUT_FILE)
  except HttpError, e:
    print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
  
  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime)
