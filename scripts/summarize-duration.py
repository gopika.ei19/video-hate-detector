#!/usr/bin/python

'''
This script summarizes the duration for a list of videos and saves it in a csv.
'''

from __future__ import unicode_literals
import datetime
import youtube_dl
import json
import os
import sys
from vhd_utils import *
import isodate
import collections

def filter_converted_duration(video):
  google_duration = video['contentDetails']['duration']
  duration = isodate.parse_duration(google_duration)
  return duration.total_seconds()

##### MAIN #####

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 3:
    print 'ERROR: You must provide an input file and a destination CSV file. The file should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python summarize-duration.py input.json output.csv\n'
  else:
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    
    videos = load_json(sys.argv[1])
    durations = map(filter_converted_duration, videos)
    durations_frequency = collections.Counter(durations)
    print "Frequency set size: %d" % len(durations_frequency)
    durations_set = sorted(durations_frequency.keys())
    videos_counter = 0
    half_duration = 0.0
    total_videos = len(videos)
    with open(output_file, 'w') as outfile:
      for duration in durations_set:
		    outfile.write("%.2f, %d\n" % (duration/60, durations_frequency[duration]))
		    if videos_counter < total_videos/2:
		      videos_counter += durations_frequency[duration]
		      half_duration = duration/60
		      
    print "About half of the videos have duration up to %.2f minutes." % half_duration

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime) 

